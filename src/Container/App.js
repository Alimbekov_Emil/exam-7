import React, { useState } from "react";
import "./App.css";
import AddItems from "../Component/AddItems/AddItems";
import OrderDetails from "../Component/OrderDetails/OrderDetails";
import TotalPrice from "../Component/TotalPrice/TotalPrice";
import OrderEmpty from "../Component/OrderEmpty/OrderEmpty";

const MENU = [
  { name: "Hamburger", price: 80, image: <i className="fas fa-utensils"></i> },
  {name: "Cheeseburger",price: 90,image: <i className="fas fa-utensils"></i>},
  { name: "Fries", price: 45, image: <i className="fas fa-utensils"></i> },
  { name: "Coffee", price: 70, image: <i className="fas fa-coffee"></i> },
  { name: "Tea", price: 50, image: <i className="fas fa-coffee"></i> },
  { name: "Cola", price: 40, image: <i className="fas fa-coffee"></i> },
];

const App = () => {
  const [menu, setMenu] = useState([
    { name: "Hamburger", count: 0, price: 80 },
    { name: "Cheeseburger", count: 0, price: 90 },
    { name: "Fries", count: 0, price: 45 },
    { name: "Coffee", count: 0, price: 70 },
    { name: "Tea", count: 0, price: 50 },
    { name: "Cola", count: 0, price: 40 },
  ]);

  const addItems = MENU.map((item, index) => {
    return (
      <AddItems
        name={item.name}
        price={item.price}
        icons={item.image}
        key={index}
        counter={() => counterOrder(index)}
      />
    );
  });
  let empty = 0;

  const orderDetails = menu.map((order, index) => {
    if (order.count !== 0) {
      empty++;
      return (
        <OrderDetails
          key={index}
          name={order.name}
          count={order.count}
          cleanOrder={() => cleanOrder(index)}
          counterOrder={() => counterOrder(index)}
          price={order.price}
        />
      );
    }
    return "";
  });

  const counterOrder = (index) => {
    const menuCopy = [...menu];
    menuCopy[index].count++;
    setMenu(menuCopy);
  };

  const cleanOrder = (index) => {
    const menuCopy = [...menu];
    if (menuCopy[index].count !== 0) {
      menuCopy[index].count--;
    }
    setMenu(menuCopy);
  };

  const total = () => {
    return menu.reduce((acc, order) => {
      if (order.name.indexOf("Hamburger") !== -1) {
        return acc + order.count * order.price;
      } else if (order.name.indexOf("Cheeseburger") !== -1) {
        return acc + order.count * order.price;
      } else if (order.name.indexOf("Fries") !== -1) {
        return acc + order.count * order.price;
      } else if (order.name.indexOf("Coffee") !== -1) {
        return acc + order.count * order.price;
      } else if (order.name.indexOf("Tea") !== -1) {
        return acc + order.count * order.price;
      } else if (order.name.indexOf("Cola") !== -1) {
        return acc + order.count * order.price;
      }
      return acc;
    }, 0);
  };

  return (
    <div className="App">
      <div className="Order-Details">
        <OrderEmpty
          text1="Order is Empty!"
          text2="Please add some items!"
          empty={empty}
        />
        {orderDetails}
        <TotalPrice total={total} />
      </div>
      <div className="AddItems">{addItems}</div>
    </div>
  );
};

export default App;
