import React from "react";
import "./OrderEmpty.css";
const OrderEmpty = (props) => {
  if (props.empty !== 0) {
    return "";
  }

  return (
    <div className="EmptyBlock">
      <p>{props.text1}</p>
      <p>{props.text2}</p>
    </div>
  );
};

export default OrderEmpty;
