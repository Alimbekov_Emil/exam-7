import React from "react";
import "./AddItems.css";

const AddItems = (props) => {
  return (
    <div className="Item" onClick={props.counter}>
      <b>{props.name}</b>
      <p>Price:{props.price}</p>
      <div>{props.icons}</div>
    </div>
  );
};

export default AddItems;
