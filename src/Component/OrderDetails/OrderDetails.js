import React from "react";
import "./OrderDetails.css";
import CleanButton from "../CleanButton/CleanButton";
import AddButton from "../AddButton/AddButton";

const OrderDetails = (props) => {
  return (
    <div className="Order">
      <span>{props.name}</span>
      <span>X {props.count}</span>
      <span>{props.price}KGS</span>
      <CleanButton onClick={props.cleanOrder} />
      <AddButton onClick={props.counterOrder} />
    </div>
  );
};

export default OrderDetails;
