import React from "react";
import "./CleanButtom.css";
const CleanButton = (props) => {
  return (
    <button className="CleanButton" onClick={props.onClick}>
      -
    </button>
  );
};

export default CleanButton;
