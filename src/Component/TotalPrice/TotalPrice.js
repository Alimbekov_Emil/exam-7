import React from "react";
import "./TotalPrice.css";

const TotalPrice = (props) => {
  return <div className="Total">Total:{props.total()}</div>;
};

export default TotalPrice;
